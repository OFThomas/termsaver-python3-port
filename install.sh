######################################################################
# @author      : oliver (oliver@oliver-pc)
# @file        : install
# @created     : Tuesday May 10, 2022 22:01:28 BST
#
# @description : 
######################################################################
BASEDIR=$(pwd)
cd termsaver
sudo python3 setup.py install

cd ${BASEDIR}

