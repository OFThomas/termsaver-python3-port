######################################################################
# @author      : oliver (oliver@oliver-pc)
# @file        : remove
# @created     : Tuesday May 10, 2022 22:02:20 BST
#
# @description : 
######################################################################

sudo rm -rf /usr/local/bin/termsaver
sudo rm -rf /usr/local/lib/python3.9/dist-packages/termsaver-0.4-py3.9.egg
#sudo rm -rf /usr/local/share/locale/en/LC_MESSAGES/termsaver.mo
#sudo rm -rf /usr/local/share/locale/ja/LC_MESSAGES/termsaver.mo
#sudo rm -rf /usr/local/share/locale/pt/LC_MESSAGES/termsaver.mo


